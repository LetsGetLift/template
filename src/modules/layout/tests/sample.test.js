import * as logic from "../logic";

describe(`Test Converter logic`, () => {
  it(`5+5=10`, () => {
    const a = 5;
    const b = 5;
    const expected = 10;
    const functionResult = logic.sum(a, b);
    expect(expected).equal(functionResult);
  });
});
