import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './modules/layout/Layout';
import './styles/index.less';
import '../assets/ava.png';

ReactDOM.render(<Layout />,  document.getElementById('root'),
);
